import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: '首页',
    hidden: true,
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index')
    }]
  },
  {
    path: '/Academy',
    component: Layout,
    redirect: '/Academy/Academylist',
    name: '学院管理',
    meta: { title: '学院管理', icon: 'example' },
    children: [
      {
        path: 'Academylist',
        name: '学院列表',
        component: () => import('@/views/edu/Academy/list'),
        meta: { title: '学院列表', icon: 'table' }
      },

    ]
  },
  {
    path: '/teacher',
    component: Layout,
    redirect: '/teacher/teacherlist',
    name: '教师管理',
    meta: { title: '教师管理', icon: 'example' },
    children: [
      {
        path: 'teacherlist',
        name: '教师列表',
        component: () => import('@/views/edu/teacher/list'),
        meta: { title: '教师列表', icon: 'table' }
      },
      {
        path: 'add',
        name: '教师添加',
        component: () => import('@/views/edu/teacher/add'),
        meta: { title: '添加教师', icon: 'tree' }
      },
      {
        path: 'add/:id',
        name: '教师修改',
        component: () => import('@/views/edu/teacher/add'),
        meta: { title: '教师修改', noCache:true },
        hidden:true
      },


    ]
  },
  {
    path: '/teacherGroup',
    component: Layout,
    redirect: '/teacherGroup/teacherGrouplist',
    name: '教师组管理',
    meta: { title: '教师组管理', icon: 'example' },
    children: [
      {
        path: 'teacherGrouplist',
        name: '教师组列表',
        component: () => import('@/views/edu/teacherGrouplist/list'),
        meta: { title: '教师组列表', icon: 'table' }
      },
      {
        path: 'teacherGroupAdd/:id',
        name: '修改教师组',
        component: () => import('@/views/edu/teacherGrouplist/add'),
        meta: { title: '修改教师组', icon: 'table',noCache:true  },
        hidden:true
      },
      {
        path: 'teacherGroupAdd/',
        name: '新建教师组',
        component: () => import('@/views/edu/teacherGrouplist/add'),
        meta: { title: '新建教师组', icon: 'table' }
      },
    ]
  },
  {
    path: '/student',
    component: Layout,
    redirect: '/student/studentlist',
    name: '学生管理',
    meta: { title: '学生管理', icon: 'example' },
    children: [
      {
        path: 'studentlist',
        name: '学生列表',
        component: () => import('@/views/edu/student/list'),
        meta: { title: '学生列表', icon: 'table' }
      },
      {
        path: 'add',
        name: '学生添加',
        component: () => import('@/views/edu/student/add'),
        meta: { title: '添加学生', icon: 'tree' }
      },
      {
        path: 'add/:id',
        name: '学生修改',
        component: () => import('@/views/edu/student/add'),
        meta: { title: '学生修改', noCache:true },
        hidden:true
      }
    ]
  },
  {
    path: '/subject',
    component: Layout,
    redirect: '/subject/list',
    name: '课程分类管理',
    meta: { title: '课程分类管理', icon: 'example' },
    children: [
      {
        path: 'list',
        name: '课程分类列表',
        component: () => import('@/views/edu/subject/list'),
        meta: { title: '课程分类列表', icon: 'table' }
      },
      {
        path: 'save',
        name: '添加课程分类',
        component: () => import('@/views/edu/subject/add'),
        meta: { title: '添加课程分类', icon: 'tree' }
      }
    ]
  },
  {
    path: '/course',
    component: Layout,
    redirect: '/course/list',
    name: 'Course',
    meta: { title: '课程管理', icon: 'form' },
    children: [
      {
        path: 'list',
        name: 'EduCourseList',
        component: () => import('@/views/edu/course/list'),
        meta: { title: '课程列表' }
      },
      {
        path: 'info',
        name: 'EduCourseInfo',
        component: () => import('@/views/edu/course/info'),
        meta: { title: '发布课程' }
      },
      {
        path: 'info/:id',
        name: 'EduCourseInfoEdit',
        component: () => import('@/views/edu/course/info'),
        meta: { title: '编辑课程基本信息', noCache: true },
        hidden: true
      },
      {
        path: 'chapter/:id',
        name: 'EduCourseChapterEdit',
        component: () => import('@/views/edu/course/chapter'),
        meta: { title: '编辑课程大纲', noCache: true },
        hidden: true
      },
      {
        path: 'publish/:id',
        name: 'EduCoursePublishEdit',
        component: () => import('@/views/edu/course/publish'),
        meta: { title: '发布课程', noCache: true },
        hidden: true
      }
    ]
  },
  {
    path: '/statistics/daily',
    component: Layout,
    redirect: '/statistics/daily/create',
    name: 'Statistics',
    meta: { title: '统计分析', icon:  'form' },
    children: [
      {
        path: 'create',
        name: 'StatisticsDailyCreate',
        component: () => import('@/views/statistics/daily/create'),
        meta: { title: '生成统计' }
      },
      {
        path: 'chart',
        name: 'StatisticsDayChart',
        component: () => import('@/views/statistics/daily/show'),
        meta: { title: '统计图表' }
      },
    ]
  },


  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
