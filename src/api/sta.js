import request from '@/utils/request'


export default {

  createStaData(day) {
    return request({
      url: `/statistics/edu-statistics_daily/registerCount/${day}`,
      method: 'post'
    })
  },
  //2 获取统计数据
  getDataSta(searchObj) {
    return request({
      url: `/statistics/edu-statistics_daily/showData/${searchObj.type}/${searchObj.begin}/${searchObj.end}`,
      method: 'get'
    })
  }
}
