import request from '@/utils/request'

export default {

  addTeacherGroup(teacherGroup) {
    return request({
      url:'/eduservice/edu_teachergroup/',
      method: 'post',
      data: teacherGroup
    })
  },

  //修改章节
  updateTeacherGroup(teacherGroup) {
    return request({
      url: `/eduservice/edu_teachergroup/`,
      method: 'put',
      data: teacherGroup
    })
  },
  //删除章节
  deleteTeacherGroup(id) {
    return request({
      url: `/eduservice/edu_teachergroup/${id}`,
      method: 'delete'
    })
  },
  getByTeacherGroupById(id){
    return request({
      url:`/eduservice/edu_teachergroup/${id}`,
      method:'get'
    })
  },
  getAllTeacherGroupList(academyId){
    return request({
      url:`/eduservice/edu_teachergroup/getByAcaId/${academyId}`,
      method:'get'
    })
  },
  getPageList(page, limit, params){
    return request({
      url: `/eduservice/edu_teachergroup/pageStudentCondition/${page}/${limit}`,
      method: 'post',
      data: params
    })
  },
  getTeachersById(id){
    return request({
      url:`/eduservice/edu_teachergroup/getTeacherList/${id}`,
      method:'get'
    })
  },
  saveTeacherAndGroup(groupAndTeacher){
    return request({
      url:`/eduservice/edu-teachergroup-and-teacher/`,
      method:'post',
      data:groupAndTeacher
    })
  },
  delete(id){
    return request({
      url:`/eduservice/edu-teachergroup-and-teacher/${id}`,
      method:'delete',

    })
  },
}
