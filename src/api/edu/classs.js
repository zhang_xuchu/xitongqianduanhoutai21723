import request from '@/utils/request'

export default {
  addClasss(classs) {
    return request({
      url:'/eduservice/edu_classs/',
      method: 'post',
      data: classs
    })
  },

  updateClasss(classs) {
    return request({
      url: `/eduservice/edu_classs/`,
      method: 'put',
      data: classs
    })
  },

  deleteClasss(id) {
    return request({
      url: `/eduservice/edu_classs/${id}`,
      method: 'delete'
    })
  },
  getByClasssById(id){
    return request({
      url:`/eduservice/edu_classs/${id}`,
      method:'get'
    })
  },
  getClassList(page, limit, params){
    return request({
      url: `/eduservice/edu-teacher/pageTeacherCondition/${page}/${limit}`,
      method: 'post',
      data: params
    })
  },
  getClassBymajor(majorId){
    return request({
      url:`/eduservice/edu_classs/getAllByMajorId/${majorId}`,
      method:'get'
    })
  },
///////////////////////////////
  getClassByteacher(teacherId){
    return request({
      url:`/eduservice/edu_teacher_class/getByTeacher/${teacherId}`,
      method:'get'
    })
  },
  getClassByclasss(classsId){
    return request({
      url:`/eduservice/edu_teacher_class/getByclasss/${classsId}`,
      method:'get'
    })
  },
  addTeacher_class(teacherClass) {
    return request({
      url:'/eduservice/edu_teacher_class/',
      method: 'post',
      data: teacherClass
    })
  },
}
