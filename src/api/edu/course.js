import request from '@/utils/request'
export default {
  saveCourseInfo(courseInfo) {
    return request({
      url: `/eduservice/edu-course/`,
      method: 'post',
      data: courseInfo
    })
  },
  deleteCourse(id) {
    return request({
      url: `/eduservice/edu-course/${id}`,
      method: 'delete',

    })
  },
  updateCourseInfo(courseInfoForm,id) {
    return request({
      url: `/eduservice/edu-course/${id}`,
      method: 'put',
      data:courseInfoForm

    })
  },
  getCoursePublishVo(id) {
    return request({
      url: `/eduservice/edu-course/course_publish_info/${id}`,
      method: 'get',
    })
  },
  publish(id) {
    return request({
      url: `/eduservice/edu-course/publish/${id}`,
      method: 'post',
    })
  },

  CourseList(page,limit,searchObj){
    return request({
      url: `/eduservice/edu-course/${page}/${limit}`,
      method: 'get',
      params: searchObj
    })
  },
  getCourseInfo(id) {
    return request({
      url: `/eduservice/edu-course/${id}`,
      method: 'get',

    })
  },
}
