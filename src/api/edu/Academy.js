import request from '@/utils/request'

export default {
  getAll() {
    return request({
      url: `/eduservice/edu_academy/findAll`,
      method: 'get'
    })
  },

  addAcademy(academy) {
    return request({
      url:'/eduservice/edu_academy/',
      method: 'post',
      data: academy
    })
  },

  //修改章节
  updateAcademy(academy) {
    return request({
      url: `/eduservice/edu_academy/`,
      method: 'put',
      data: academy
    })
  },
  //删除章节
  deleteAcademy(chapterId) {
    return request({
      url: `/eduservice/edu_academy/${chapterId}`,
      method: 'delete'
    })
  },
  getByAcademyById(id){
    return request({
      url:`/eduservice/edu_academy/${id}`,
      method:'get'
    })
  },
  getAllAcaList(){
    return request({
      url:`/eduservice/edu_academy/getAll`,
      method:'get'
    })
  },
}
