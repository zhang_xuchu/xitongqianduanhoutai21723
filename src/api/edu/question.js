import request from '@/utils/request'
export default {
  addQuestion(question) {
    return request({
      url: `/eduservice/edu_question/`,
      method: 'post',
      data: question
    })
  },
  getQuestios(chapterId){
    return request({
      url:`/eduservice/edu_question/${chapterId}`,
      method:'get'
    })
  },
  deleteQuestion(id) {
    return request({
      url: `/eduservice/edu_question/${id}`,
      method: 'delete',

    })
  },
  updateQuestion(question) {
    return request({
      url: `/eduservice/edu_question/`,
      method: 'put',
      data: question
    })
  },
  addChoiceQuestion(questionChoice) {
    return request({
      url: `/eduservice/edu_question_choice/`,
      method: 'post',
      data: questionChoice
    })
  },

  deleteChoiceQuestion(id) {
    return request({
      url: `/eduservice/edu_question_choice/${id}`,
      method: 'delete',

    })
  },

}
