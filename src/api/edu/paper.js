import request from '@/utils/request'
export default {
  addPaper(paperVo) {
    return request({
      url: `/eduservice/edu_paper/`,
      method: 'post',
      data: paperVo
    })
  },

  getPaper(id){
    return request({
      url:`/eduservice/edu_paper/${id}`,
      method:'get'
    })
  },
  deletePaper(id) {
    return request({
      url: `/eduservice/edu_paper/${id}`,
      method: 'delete',

    })
  },
  updatePaper(papervo) {
    return request({
      url: `/eduservice/edu_paper/`,
      method: 'put',
      data: papervo
    })
  },

  addPaperAnswer(paperAnswerVo) {
    return request({
      url: `/eduservice/edu_paper_answer`,
      method: 'post',
      data: paperAnswerVo
    })
  },
  getPaperAnswer(id){
    return request({
      url:`/eduservice/edu_paper_answer/${id}`,
      method:'get'

    })
  },
  deletePaperAnswer(id) {
    return request({
      url: `/eduservice/edu_paper_answer/${id}`,
      method: 'delete',

    })
  },
  updatePaperAnswer(papervo) {
    return request({
      url: `/eduservice/edu_paper_answer`,
      method: 'put',
      data: papervo
    })
  },
}
