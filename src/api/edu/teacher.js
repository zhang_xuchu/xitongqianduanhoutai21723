import request from '@/utils/request'

export default {
  save(teacher) {
    return request({
      url: '/eduservice/edu-teacher/',
      method: 'post',
      data: teacher
    })
  },
  removeById(teacherId) {
    return request({
      url: `/eduservice/edu-teacher/${teacherId}`,
      method: 'delete'
    })
  },
  updateById(teacher) {
    return request({
      url: `/eduservice/edu-teacher/${teacher.id}`,
      method: 'put',
      data: teacher
    })
  },
  getById(id) {
    return request({
      url: `/eduservice/edu-teacher/${id}`,
      method: 'get'
    })
  },
  getPageList(page, limit, params){
    return request({
      url: `/eduservice/edu-teacher/pageTeacherCondition/${page}/${limit}`,
      method: 'post',
      data: params
    })
  },
  getListTeacher(){
    return request({
      url:'/eduservice/edu-teacher/findAll',
      method:'get',
    })
  }

}
