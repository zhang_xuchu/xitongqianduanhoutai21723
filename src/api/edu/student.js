import request from '@/utils/request'

export default {
  save(student) {
    return request({
      url: '/eduservice/edu_student/',
      method: 'post',
      data: student
    })
  },
  removeById(id) {
    return request({
      url: `/eduservice/edu_student/${id}`,
      method: 'delete'
    })
  },
  updateById(student) {
    return request({
      url: `/eduservice/edu_student/${student.id}`,
      method: 'put',
      data: student
    })
  },
  getById(id) {
    return request({
      url: `/eduservice/edu_student/${id}`,
      method: 'get'
    })
  },
  getPageList(page, limit, params){
    return request({
      url: `/eduservice/edu_student/pageStudentCondition/${page}/${limit}`,
      method: 'post',
      data: params
    })
  },
  getListTeacher(){
    return request({
      url:'/eduservice/edu_student/findAll',
      method:'get',
    })
  }

}
