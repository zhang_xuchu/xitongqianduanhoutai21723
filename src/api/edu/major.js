import request from '@/utils/request'

export default {
  addMajor(major) {
    return request({
      url:'/eduservice/edu_major/',
      method: 'post',
      data: major
    })
  },

  //修改章节
  updateMajor(major) {
    return request({
      url: `/eduservice/edu_major/`,
      method: 'put',
      data: major
    })
  },
  //删除章节
  deleteMajor(id) {
    return request({
      url: `/eduservice/edu_major/${id}`,
      method: 'delete'
    })
  },
  getByMajorById(id){
    return request({
      url:`/eduservice/edu_major/${id}`,
      method:'get'
    })
  },
  getClassByAcaId(AcaId){
    return request({
      url:`/eduservice/edu_major/getAllByAcaId/${AcaId}`,
      method:'get'
    })
  },
}
