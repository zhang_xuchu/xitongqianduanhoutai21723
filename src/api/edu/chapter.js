import request from '@/utils/request'

export default {
  getNestedTreeList(courseId) {
    return request({
      url: `/eduservice/edu_chapter/findCourseList/${courseId}`,
      method: 'get'
    })
  },
  //添加章节
  addChapter(chapter) {
    return request({
      url:'/eduservice/edu_chapter/',
      method: 'post',
      data: chapter
    })
  },

  //修改章节
  updateChapter(chapter) {
    return request({
      url: `/eduservice/edu_chapter/`,
      method: 'put',
      data: chapter
    })
  },
  //删除章节
  deleteChapter(chapterId) {
    return request({
      url: `/eduservice/edu_chapter/${chapterId}`,
      method: 'delete'
    })
  },
  getByChapterById(chapterId){
    return request({
      url:`/eduservice/edu_chapter/${chapterId}`,
      method:'get'
    })
  }
}
